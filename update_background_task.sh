#!/bin/bash

notify-send "Fetching the new background image..."

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")

source $SCRIPTPATH/venv/bin/activate
python $SCRIPTPATH/nasa_feed_parser.py

convert /home/xonecas/.backgrounds/background.jpeg -gravity center -crop 2560x1440+0+0 /home/xonecas/.backgrounds/background.jpeg
convert /home/xonecas/.backgrounds/background.jpeg /home/xonecas/.backgrounds/background.png
feh --bg-scale /home/xonecas/.backgrounds/background.jpeg