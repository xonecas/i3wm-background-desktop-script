import errno
import re
import feedparser
import os
import random
from urllib.request import urlretrieve, urlopen

# @TODO(sean): Create a cache...

IMAGES_LOCATION = os.path.expanduser("~") + "/.backgrounds/"


def use_NASA_image_of_the_day():
    """Use the NASA image of the day feed to get a background"""

    NASA_IMAGES_URL = "https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss"

    feed = feedparser.parse(NASA_IMAGES_URL)
    random_entry = random.choice(feed["entries"])
    random_entry_img = None
    for link in random_entry["links"]:
        if link["rel"] == "enclosure":
            random_entry_img = link["href"]

    urlretrieve(random_entry_img, f"{IMAGES_LOCATION}background.jpeg")


def use_earth_observatory_image_of_the_day():
    """Use the observatory's image of the day feed"""

    OBSERVATORY_IMAGES_URL = (
        "https://earthobservatory.nasa.gov/feeds/image-of-the-day.rss"
    )

    feed = feedparser.parse(OBSERVATORY_IMAGES_URL)
    random_item = random.choice(feed["entries"])
    resp = urlopen(random_item["link"])
    content = resp.read().decode("utf8")

    random_item_img = re.findall(
        r"(?P<jpg>https?://[^\s]+lrg\.jpg)", content
    )  # Assume the first link is the correct one...

    urlretrieve(random_item_img[0], f"{IMAGES_LOCATION}background.jpeg")


def main():
    """Get the latest image from NASA Images."""

    try:
        os.makedirs(IMAGES_LOCATION)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    image = use_NASA_image_of_the_day()
    # image = use_earth_observatory_image_of_the_day()


if __name__ == "__main__":
    main()
